package com.lms.lms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import com.lms.dto.ApplyLeaveResponseDto;
import com.lms.dto.LeaveRequestDto;
import com.lms.entity.Employee;
import com.lms.entity.LeaveHistory;
import com.lms.entity.LeaveType;
import com.lms.exceptions.DateHolidayException;
import com.lms.exceptions.LeaveInsufficientException;
import com.lms.repository.AnnualLeaveRepository;
import com.lms.repository.EmployeeRepository;
import com.lms.repository.LeaveHistoryRepository;
import com.lms.repository.LeaveTypeRepository;
import com.lms.serviceimpl.ApplyLeaveServiceImpl;

@SpringBootTest
public class LeaveApplyTests {
	
	@Mock
	LeaveTypeRepository leaveTypeRepository;
	@Mock
	EmployeeRepository employeeRepository;
	@Mock
	LeaveHistoryRepository leaveHistoryRepository;
	@Mock
	AnnualLeaveRepository annualLeaveRepository;
	@InjectMocks
	ApplyLeaveServiceImpl leaveServiceImpl;
	
	LeaveType leavetypeone;
	LeaveType leavetypetwo;
	Employee emplyoeeone;
	LeaveRequestDto leaveRequestDto;
	ApplyLeaveResponseDto applyLeaveResponseDto;
	LeaveHistory leaveHistory;
	@BeforeEach
	public void setup()
	{
		leavetypeone=new LeaveType();
		leavetypeone.setLeaveCategory("general");
		leavetypeone.setLeavecount(10);
		leavetypeone.setLeaveId(1);
		leavetypetwo=new LeaveType();
		emplyoeeone=new Employee();
		emplyoeeone.setEmployeeId(1);
		emplyoeeone.setEmail("han@gmail.com");
		emplyoeeone.setName("hanumantha");
		emplyoeeone.setPhonenumber("966995345");
		leavetypeone.setEmployee(emplyoeeone);
		leaveRequestDto=new LeaveRequestDto();
		applyLeaveResponseDto=new ApplyLeaveResponseDto();
		leaveRequestDto.setEmployeeId(1);
		leaveRequestDto.setFromdate(LocalDate.of(2020, Month.MARCH, 28));
		leaveRequestDto.setLeaveCategory("general");
		leaveHistory=new LeaveHistory();
		
	}
	@Test
	public void applyLeaveNegitiveCasetwo()
	{
		leaveRequestDto.setTodate((LocalDate.of(2020, Month.MARCH, 28)));
		Mockito.when(employeeRepository.findByEmployeeId(Mockito.anyInt())).thenReturn(emplyoeeone);
		Mockito.when(leaveTypeRepository.findByEmployee_employeeIdAndLeaveCategory(Mockito.anyInt(),Mockito.anyString())).thenReturn(leavetypeone);
		Mockito.when(annualLeaveRepository.existsByLocaldate(LocalDate.of(2020, Month.MARCH, 28))).thenReturn(false);
		assertThrows(DateHolidayException.class, ()->leaveServiceImpl.applyLeave(leaveRequestDto));

	}
	@Test
	public void applyLeaveNegitiveCase()
	{
		leaveRequestDto.setTodate(LocalDate.now().plusDays(20));
		Mockito.when(employeeRepository.findByEmployeeId(Mockito.anyInt())).thenReturn(emplyoeeone);
		Mockito.when(leaveTypeRepository.findByEmployee_employeeIdAndLeaveCategory(Mockito.anyInt(),Mockito.anyString())).thenReturn(leavetypeone);
		assertThrows(LeaveInsufficientException.class, ()->leaveServiceImpl.applyLeave(leaveRequestDto));

	}
	@Test
	public void applyLeavePositiveCase() throws LeaveInsufficientException, DateHolidayException
	{
		leaveHistory.setEmployee(emplyoeeone);
		leaveHistory.setCount(10);
		leaveHistory.setFromdate(leaveRequestDto.getFromdate());
		leaveHistory.setTodate(leaveRequestDto.getTodate());
		leaveHistory.setReason("festival");
		leaveHistory.setStatus("submitted");
		leaveHistory.setTime(LocalDateTime.now());
		leaveRequestDto.setTodate(LocalDate.now().plusDays(6));
		Mockito.when(employeeRepository.findByEmployeeId(Mockito.anyInt())).thenReturn(emplyoeeone);
		Mockito.when(leaveTypeRepository.findByEmployee_employeeIdAndLeaveCategory(Mockito.anyInt(),Mockito.anyString())).thenReturn(leavetypeone);
		Mockito.when(leaveHistoryRepository.save(Mockito.any(LeaveHistory.class))).thenReturn(leaveHistory);
		Mockito.when(leaveTypeRepository.save(Mockito.any(LeaveType.class))).thenReturn(leavetypeone);
		assertEquals(HttpStatus.OK.value(),leaveServiceImpl.applyLeave(leaveRequestDto).getBody().getStatuscode());

	}

}
