package com.lms.lms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import com.lms.dto.LeaveApprovalDto;
import com.lms.dto.LeaveApprovalRequestDto;
import com.lms.entity.Employee;
import com.lms.entity.LeaveHistory;
import com.lms.entity.LeaveStatus;
import com.lms.exceptions.LeaveNotFoundException;

import com.lms.repository.LeaveHistoryRepository;
import com.lms.repository.LeaveStatusRepository;
import com.lms.repository.LeaveTypeRepository;
import com.lms.serviceimpl.ApprovalLeaveServiceImpl;

@SpringBootTest
public class LeaveStatusTests {

	@Mock
	LeaveTypeRepository leaveTypeRepository;
	@Mock
	LeaveStatusRepository leaveStatusRepository;
	@InjectMocks
	ApprovalLeaveServiceImpl approvalLeaveService;
	@Mock
	LeaveHistoryRepository leaveHistoryRepository;
	LeaveHistory leaveHistory;
	LeaveHistory leaveHistorytwo;
	Employee emplyoeeone;
	LeaveApprovalRequestDto leaveApprovalRequestDto;
	LeaveApprovalDto leaveApprovalDto;
	LeaveStatus leavestatus;

	@BeforeEach
	public void setup() {
		leaveHistory = new LeaveHistory();
		leaveHistory.setCount(10);
		leaveHistory.setHistoryId(1);
		leaveHistory.setFromdate(LocalDate.now());
		leaveHistory.setTodate(LocalDate.now().plusDays(2));
		leaveHistory.setReason("festival");
		leaveHistory.setStatus("submitted");
		leaveHistory.setTime(LocalDateTime.now());
		leaveApprovalRequestDto = new LeaveApprovalRequestDto();
		leaveApprovalDto = new LeaveApprovalDto();
		leavestatus = new LeaveStatus();

	}

	@Test
	public void leaveApprovalNegitive() throws LeaveNotFoundException {
		leaveHistorytwo = new LeaveHistory();
		leaveApprovalRequestDto.setEmployeeid(1);
		leaveApprovalRequestDto.setLeaveHistoryId(1);
		Mockito.when(leaveHistoryRepository.findByHistoryId(Mockito.anyInt())).thenReturn(leaveHistorytwo);

		assertThrows(LeaveNotFoundException.class,
				() -> approvalLeaveService.leaveapprovalrequest(leaveApprovalRequestDto));
	}

	@Test
	public void leaveApprovalPositive() throws LeaveNotFoundException {

		leaveApprovalRequestDto.setEmployeeid(1);
		leaveApprovalRequestDto.setLeaveHistoryId(1);
		leavestatus.setApproveEmployeeid(1);
		leavestatus.setLeaveApprovalStatus("Approved");
		leavestatus.setLeaveHistory(leaveHistory);
		leavestatus.setComment("no issues");
		Mockito.when(leaveHistoryRepository.findByHistoryId(Mockito.anyInt())).thenReturn(leaveHistory);
		Mockito.when(leaveStatusRepository.save(Mockito.any(LeaveStatus.class))).thenReturn(leavestatus);

		assertEquals(HttpStatus.OK.value(),
				approvalLeaveService.leaveapprovalrequest(leaveApprovalRequestDto).getBody().getStatuscode());
	}
}
