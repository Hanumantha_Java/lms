package com.lms.lms;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.lms.dto.LeaveResponseDto;
import com.lms.entity.Employee;
import com.lms.entity.LeaveType;
import com.lms.exceptions.LeaveNotFoundException;
import com.lms.repository.EmployeeRepository;
import com.lms.repository.LeaveTypeRepository;
import com.lms.serviceimpl.LeaveServiceImpl;

@SpringBootTest
class LmsApplicationTests {

	@Mock
	LeaveTypeRepository leaveTypeRepository;
	@Mock
	EmployeeRepository employeeRepository;
	@InjectMocks
	LeaveServiceImpl leaveServiceImpl;
	
	LeaveType leavetypeone;
	LeaveType leavetypetwo;
	Employee emplyoeeone;
	List<LeaveType> leavecategorylist;
	LeaveResponseDto leaveresponseDto;
	Map<String,Integer> eachcount;
	@BeforeEach
	public void setup()
	{
		leavetypeone=new LeaveType();
		leavetypeone.setLeaveCategory("general");
		leavetypeone.setLeavecount(10);
		leavetypeone.setLeaveId(1);
		leavetypetwo=new LeaveType();
		leaveresponseDto=new LeaveResponseDto();
		eachcount=new HashMap<String, Integer>();
		leavecategorylist=new ArrayList<LeaveType>();
		emplyoeeone=new Employee();
		emplyoeeone.setEmployeeId(1);
		leavetypeone.setEmployee(emplyoeeone);

	}
	
	@Test
	public void findLeaveNagativeCase()throws LeaveNotFoundException
	{
		int employeeid=1;
		Mockito.when(leaveTypeRepository.findAllByEmployee_employeeId(Mockito.anyInt())).thenReturn(leavecategorylist);
		
		assertThrows(LeaveNotFoundException.class, ()->leaveServiceImpl.findallleave(employeeid));
	}
	@Test
	public void findLeavePositive()throws LeaveNotFoundException
	{
		int employeeid=1;
		int leaveCount=10;
		leavecategorylist.add(leavetypeone);
		eachcount.put("general",10);
		Mockito.when(leaveTypeRepository.findAllByEmployee_employeeId(Mockito.anyInt())).thenReturn(leavecategorylist);
		leaveresponseDto.setEmployeeId(employeeid);
		leaveresponseDto.setLeaveCount(leaveCount);
		assertEquals(leaveresponseDto.getLeaveCount(),leaveServiceImpl.findallleave(employeeid).getBody().getLeaveCount());
	}

}
