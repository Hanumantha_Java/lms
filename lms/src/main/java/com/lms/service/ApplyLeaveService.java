package com.lms.service;

import org.springframework.http.ResponseEntity;

import com.lms.dto.ApplyLeaveResponseDto;
import com.lms.dto.LeaveRequestDto;
import com.lms.exceptions.DateHolidayException;
import com.lms.exceptions.LeaveInsufficientException;

public interface ApplyLeaveService {
	public ResponseEntity<ApplyLeaveResponseDto> applyLeave(LeaveRequestDto leaveRequestDto) throws LeaveInsufficientException, DateHolidayException;

}
