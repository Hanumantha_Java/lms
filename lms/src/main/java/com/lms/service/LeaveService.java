package com.lms.service;

import org.springframework.http.ResponseEntity;

import com.lms.dto.LeaveResponseDto;
import com.lms.exceptions.LeaveNotFoundException;

public interface LeaveService {

	public ResponseEntity<LeaveResponseDto> findallleave(int employeeid) throws LeaveNotFoundException;

}
