package com.lms.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lms.dto.LeaveApprovalDto;
import com.lms.dto.LeaveApprovalRequestDto;
import com.lms.exceptions.LeaveNotFoundException;

@Service
public interface ApprovalLeaveService {
	public ResponseEntity<LeaveApprovalDto> leaveapprovalrequest(LeaveApprovalRequestDto leaveapproval) throws LeaveNotFoundException;
}
