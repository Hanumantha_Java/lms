package com.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.entity.LeaveStatus;

public interface LeaveStatusRepository extends JpaRepository<LeaveStatus, Integer>{

}
