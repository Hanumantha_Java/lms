package com.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.entity.LeaveHistory;

public interface LeaveHistoryRepository extends JpaRepository<LeaveHistory, Integer>{

	LeaveHistory findByHistoryId(int leaveHistoryId);

}
