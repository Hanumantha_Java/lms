package com.lms.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


import com.lms.entity.LeaveType;

public interface LeaveTypeRepository extends JpaRepository<LeaveType, Integer>{

	List<LeaveType> findAllByEmployee_employeeId(int employeeid);

	LeaveType findByEmployee_employeeIdAndLeaveCategory(int employeeId, String leaveCategory);

}

