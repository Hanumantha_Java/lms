package com.lms.repository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lms.entity.AnnualLeave;

public interface AnnualLeaveRepository extends JpaRepository<AnnualLeave, Integer>{

	boolean existsByLocaldate(LocalDate result);

}
