package com.lms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.dto.ApplyLeaveResponseDto;
import com.lms.dto.LeaveApprovalDto;
import com.lms.dto.LeaveApprovalRequestDto;
import com.lms.dto.LeaveRequestDto;
import com.lms.dto.LeaveResponseDto;
import com.lms.exceptions.DateHolidayException;
import com.lms.exceptions.LeaveInsufficientException;
import com.lms.exceptions.LeaveNotFoundException;
import com.lms.service.ApplyLeaveService;
import com.lms.service.ApprovalLeaveService;
import com.lms.service.LeaveService;

@RequestMapping("/lms")
@RestController
public class LmsController {

	@Autowired
	LeaveService leaveService;
	@Autowired
	ApplyLeaveService applyLeaveService;
	@Autowired
	ApprovalLeaveService approvalLeaveService;

	/*
	 * @param(employeeid)
	 */
	@GetMapping("/leaves/{employeeid}")
	public ResponseEntity<LeaveResponseDto> findallleave(@PathVariable int employeeid) throws LeaveNotFoundException {
		return leaveService.findallleave(employeeid);
	}

	/*
	 * @param(employeeId,fromdate,todate,leaveCategory)
	 */
	@PostMapping("applyleaves")
	public ResponseEntity<ApplyLeaveResponseDto> applyleave(@RequestBody LeaveRequestDto leaveRequestDto)
			throws LeaveInsufficientException, DateHolidayException {
		return applyLeaveService.applyLeave(leaveRequestDto);
	}

	/*
	 * @param(employeeId,leaveHistoryId)
	 */
	@PostMapping("leaveapproval")
	public ResponseEntity<LeaveApprovalDto> leaveapproval(@RequestBody LeaveApprovalRequestDto leaveapproval)
			throws LeaveNotFoundException {
		return approvalLeaveService.leaveapprovalrequest(leaveapproval);
	}

}
