package com.lms.dto;

import java.util.Map;

public class LeaveResponseDto {
	
	private int employeeId;
	private int leaveCount;
	private int usedLeave;
		
	private Map<String,Integer> leavetype;
	public int getUsedLeave() {
		return usedLeave;
	}
	public void setUsedLeave(int usedLeave) {
		this.usedLeave = usedLeave;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getLeaveCount() {
		return leaveCount;
	}
	public void setLeaveCount(int leaveCount) {
		this.leaveCount = leaveCount;
	}
	public Map<String, Integer> getLeavetype() {
		return leavetype;
	}
	public void setLeavetype(Map<String, Integer> leavetype) {
		this.leavetype = leavetype;
	}
	

}
