package com.lms.dto;

import java.time.LocalDate;

public class LeaveRequestDto {

private int employeeId;
private LocalDate fromdate;
private LocalDate todate;
private String leaveCategory;
public int getEmployeeId() {
	return employeeId;
}
public void setEmployeeId(int employeeId) {
	this.employeeId = employeeId;
}
public LocalDate getFromdate() {
	return fromdate;
}
public void setFromdate(LocalDate fromdate) {
	this.fromdate = fromdate;
}
public LocalDate getTodate() {
	return todate;
}
public void setTodate(LocalDate todate) {
	this.todate = todate;
}
public String getLeaveCategory() {
	return leaveCategory;
}
public void setLeaveCategory(String leaveCategory) {
	this.leaveCategory = leaveCategory;
}

}

