package com.lms.dto;

public class LeaveApprovalDto {
	private int statuscode;
	private String message;
	public int getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public LeaveApprovalDto() {
	//create object
	}

}
