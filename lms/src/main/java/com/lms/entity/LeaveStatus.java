package com.lms.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class LeaveStatus implements Serializable {

	
	private static final long serialVersionUID = -90794849611061074L;
	@Id
	@GeneratedValue
	private int leaveStatusId;
	private String leaveApprovalStatus;
	private String comment;
	@Column(name="Approved_Employee_Id")
	private int approveEmployeeid;
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public LeaveHistory getLeaveHistory() {
		return leaveHistory;
	}
	public void setLeaveHistory(LeaveHistory leaveHistory) {
		this.leaveHistory = leaveHistory;
	}

	public int getLeaveStatusId() {
		return leaveStatusId;
	}
	public void setLeaveStatusId(int leaveStatusId) {
		this.leaveStatusId = leaveStatusId;
	}
	public String getLeaveApprovalStatus() {
		return leaveApprovalStatus;
	}
	public void setLeaveApprovalStatus(String leaveApprovalStatus) {
		this.leaveApprovalStatus = leaveApprovalStatus;
	}
	public int getApproveEmployeeid() {
		return approveEmployeeid;
	}
	public void setApproveEmployeeid(int approveEmployeeid) {
		this.approveEmployeeid = approveEmployeeid;
	}
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="fk_leave_history")
    LeaveHistory leaveHistory;
}
