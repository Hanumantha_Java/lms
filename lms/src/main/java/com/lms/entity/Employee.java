package com.lms.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Employee implements Serializable {

	
	private static final long serialVersionUID = -90794849611061074L;
	@Id
	@GeneratedValue
	private int employeeId;
	private String name;
	private String email;
	private String phonenumber;
	@Column(nullable = false,unique = true)
	private int supervisorId;
	
	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public Employee() {
		//create object
	}
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "employee")
	Set<LeaveType> leavetype;

	public Set<LeaveType> getLeavetype() {
		return leavetype;
	}

	public void setLeavetype(Set<LeaveType> leavetype) {
		this.leavetype = leavetype;
	}
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "employee")
	Set<LeaveHistory> leavehistory;
	public Set<LeaveHistory> getLeavehistory() {
		return leavehistory;
	}

	public void setLeavehistory(Set<LeaveHistory> leavehistory) {
		this.leavehistory = leavehistory;
	}
	

}
