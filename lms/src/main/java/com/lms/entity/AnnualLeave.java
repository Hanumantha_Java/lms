package com.lms.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AnnualLeave implements Serializable{
	

	private static final long serialVersionUID = -907948611061074L;
	@Id
	@GeneratedValue
	private int annualleaveId;
   private String description;
   private LocalDate localdate;
public int getAnnualleaveId() {
	return annualleaveId;
}
public void setAnnualleaveId(int annualleaveId) {
	this.annualleaveId = annualleaveId;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public LocalDate getLocaldate() {
	return localdate;
}
public void setLocaldate(LocalDate localdate) {
	this.localdate = localdate;
}
   
}
