package com.lms.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class LeaveType implements Serializable {

	
	private static final long serialVersionUID = -907948496161074L;
	@Id
	@GeneratedValue
	private int leaveId;
	private String leaveCategory;
	private int leavecount;
	private int usedLeave;
	
	public int getUsedLeave() {
		return usedLeave;
	}

	public void setUsedLeave(int usedLeave) {
		this.usedLeave = usedLeave;
	}

	public int getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	public String getLeaveCategory() {
		return leaveCategory;
	}

	public void setLeaveCategory(String leaveCategory) {
		this.leaveCategory = leaveCategory;
	}

	public int getLeavecount() {
		return leavecount;
	}

	public void setLeavecount(int leavecount) {
		this.leavecount = leavecount;
	}

	public LeaveType() {
//create object
	}
	@ManyToOne
	@JoinColumn(name="fk_employee")
	Employee employee;
	@JsonIgnore
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


}
