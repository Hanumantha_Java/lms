package com.lms.serviceimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lms.dto.LeaveResponseDto;
import com.lms.entity.LeaveType;
import com.lms.exceptions.LeaveNotFoundException;
import com.lms.repository.EmployeeRepository;
import com.lms.repository.LeaveTypeRepository;
import com.lms.service.LeaveService;
@Service
public class LeaveServiceImpl implements LeaveService{
	
	@Autowired
	LeaveTypeRepository leaveTypeRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	//get all leaves basesed on employee id
	public ResponseEntity<LeaveResponseDto> findallleave(int employeeid) throws LeaveNotFoundException
	{
	LeaveResponseDto leaveresponseDto=new LeaveResponseDto();
		Map<String,Integer> eachcount=new HashMap<>();
		int leaveCount=0;
		int userdLeaves=0;
		List<LeaveType> leavecategorylist=leaveTypeRepository.findAllByEmployee_employeeId(employeeid);
		if(leavecategorylist.isEmpty())
		{
			throw new LeaveNotFoundException("there is no leave records for given employee");
			}
		else
		{
			for(LeaveType leavetype : leavecategorylist)
			{
				userdLeaves+=leavetype.getUsedLeave();
				if(leavetype.getLeaveCategory().equalsIgnoreCase("RH"))
				{
					eachcount.put("RH",leavetype.getLeavecount());
					leaveCount+=leavetype.getLeavecount();
				}
				else if((leavetype.getLeaveCategory().equalsIgnoreCase("medical"))){
					eachcount.put("Medical",leavetype.getLeavecount());
					leaveCount+=leavetype.getLeavecount();
				}
				else if((leavetype.getLeaveCategory().equalsIgnoreCase("general")))
				{
					eachcount.put("general",leavetype.getLeavecount());
					leaveCount+=leavetype.getLeavecount();
				}
			}
			leaveresponseDto.setEmployeeId(employeeid);
			leaveresponseDto.setLeaveCount(leaveCount);
			leaveresponseDto.setLeavetype(eachcount);
			leaveresponseDto.setUsedLeave(userdLeaves);
			return new ResponseEntity<>(leaveresponseDto,HttpStatus.OK);
			
			
		}
	}

}
