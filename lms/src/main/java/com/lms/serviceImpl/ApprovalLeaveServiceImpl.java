package com.lms.serviceimpl;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lms.dto.LeaveApprovalDto;
import com.lms.dto.LeaveApprovalRequestDto;
import com.lms.entity.LeaveHistory;
import com.lms.entity.LeaveStatus;
import com.lms.exceptions.LeaveNotFoundException;
import com.lms.repository.LeaveHistoryRepository;
import com.lms.repository.LeaveStatusRepository;
import com.lms.service.ApprovalLeaveService;

@Service
public class ApprovalLeaveServiceImpl implements ApprovalLeaveService{

	@Autowired
	LeaveStatusRepository leaveStatusRepository;
	@Autowired
	LeaveHistoryRepository leaveHistoryRepository;
	//supervisor can approve his/her leave request
	public ResponseEntity<LeaveApprovalDto> leaveapprovalrequest(LeaveApprovalRequestDto leaveapproval) throws LeaveNotFoundException
	{
		LeaveStatus leavestatus=new LeaveStatus();
		LeaveApprovalDto leaveapprovalDto=new LeaveApprovalDto();
		LeaveHistory leaveHistory=leaveHistoryRepository.findByHistoryId(leaveapproval.getLeaveHistoryId());
	if((Objects.nonNull(leaveHistory))&&(leaveHistory.getHistoryId()!=0))
		{
	    leavestatus.setApproveEmployeeid(leaveapproval.getEmployeeid());
		leavestatus.setLeaveApprovalStatus("Approved");
		leavestatus.setLeaveHistory(leaveHistory);
		leavestatus.setComment("no issues");
	    leaveStatusRepository.save(leavestatus);
	    leaveapprovalDto.setMessage("Leave Approval status is given successsfully");
	    leaveapprovalDto.setStatuscode(HttpStatus.OK.value());
	    return new ResponseEntity<>(leaveapprovalDto,HttpStatus.OK);
	}
		else
		{
			throw new LeaveNotFoundException("leavehistory record doesn't exit");
		}
}
}

