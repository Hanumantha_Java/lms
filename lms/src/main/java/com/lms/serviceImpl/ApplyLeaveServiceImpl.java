package com.lms.serviceimpl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.lms.dto.ApplyLeaveResponseDto;
import com.lms.dto.LeaveRequestDto;
import com.lms.entity.Employee;
import com.lms.entity.LeaveHistory;
import com.lms.entity.LeaveType;
import com.lms.exceptions.DateHolidayException;
import com.lms.exceptions.LeaveInsufficientException;
import com.lms.repository.AnnualLeaveRepository;
import com.lms.repository.EmployeeRepository;
import com.lms.repository.LeaveHistoryRepository;
import com.lms.repository.LeaveTypeRepository;
import com.lms.service.ApplyLeaveService;
@Service
public class ApplyLeaveServiceImpl implements ApplyLeaveService{
	
	@Autowired
	LeaveTypeRepository leaveTypeRepository;
	@Autowired
	LeaveHistoryRepository leaveHistoryRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	AnnualLeaveRepository annualLeaveRepository;
	//apply leave and leave count will be change based on leave category
	public ResponseEntity<ApplyLeaveResponseDto> applyLeave(LeaveRequestDto leaveRequestDto) throws LeaveInsufficientException, DateHolidayException
	{
		
	Employee employee=employeeRepository.findByEmployeeId(leaveRequestDto.getEmployeeId());
	List<LeaveHistory> leavehistorys=new ArrayList<>();
	LeaveHistory leavehistory;
	ApplyLeaveResponseDto applyLeaveResponseDto=new ApplyLeaveResponseDto();
	
	LeaveType leavetype=leaveTypeRepository.findByEmployee_employeeIdAndLeaveCategory(leaveRequestDto.getEmployeeId(),leaveRequestDto.getLeaveCategory());
	int days=Period.between(leaveRequestDto.getFromdate(), leaveRequestDto.getTodate()).getDays()+1;
	 LocalDate result = leaveRequestDto.getFromdate();
	 List<LocalDate> listofdates=new ArrayList<>();
	    int addedDays = 0;
	for(int i=1;i<=days;i++)
	{
		if ((!(result.getDayOfWeek() == DayOfWeek.SATURDAY ||
	              result.getDayOfWeek() == DayOfWeek.SUNDAY))&&!(annualLeaveRepository.existsByLocaldate(result))) {
	            ++addedDays;
	            listofdates.add(result);
	        }
		result = result.plusDays(1);
	}
	if(addedDays<1)
	{
		throw new DateHolidayException("given dates are in public holiday or saturday/sunday");
	}
	if(leavetype.getLeavecount()>=addedDays)
	{
		leavetype.setLeavecount(leavetype.getLeavecount()-addedDays);
		leavetype.setUsedLeave(leavetype.getUsedLeave()+addedDays);
		for(LocalDate locadate: listofdates)
		{
		leavehistory=new LeaveHistory();
		leavehistory.setEmployee(employee);
		leavehistory.setCount(1);
		leavehistory.setFromdate(locadate);
		leavehistory.setTodate(locadate);
		leavehistory.setReason("festival");
		leavehistory.setStatus("submitted");
		leavehistory.setTime(LocalDateTime.now());
		leavehistorys.add(leavehistory);
		}
		leaveHistoryRepository.saveAll(leavehistorys);
		leaveTypeRepository.save(leavetype);
		applyLeaveResponseDto.setStatuscode(HttpStatus.OK.value());
		applyLeaveResponseDto.setMessage("Successfully applied leaves");
		
		return new ResponseEntity<>(applyLeaveResponseDto,HttpStatus.OK);
	}
	else
	{
		throw new LeaveInsufficientException("no sufficient leave to apply");
	}
	
	}
}
