package com.lms.exceptions;

public class LeaveNotFoundException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public LeaveNotFoundException() {
		super();
	}

	public LeaveNotFoundException(final String message) {
		super(message);
	}

}
