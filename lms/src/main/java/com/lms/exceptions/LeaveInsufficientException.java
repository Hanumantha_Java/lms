package com.lms.exceptions;

public class LeaveInsufficientException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public LeaveInsufficientException() {
		super();
	}

	public LeaveInsufficientException(final String message) {
		super(message);
	}


}
