package com.lms.exceptions;

public class DateHolidayException extends Exception {

	private static final long serialVersionUID = -9079454849611061074L;

	public DateHolidayException() {
		super();
	}

	public DateHolidayException(final String message) {
		super(message);
	}

}
